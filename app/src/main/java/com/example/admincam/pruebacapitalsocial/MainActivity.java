package com.example.admincam.pruebacapitalsocial;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;

import java.security.MessageDigest;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this,Login.class);
                startActivity(intent);
                finish();
            }
        },5000);

        System.out.print("KeyHashes "+KeyHashes());
    }

    public String KeyHashes(){
        PackageInfo info;
        String KeyHashes = null;
        try{
            info = getPackageManager().getPackageInfo("com.example.admincam.pruebacapitalsocial", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures){
                MessageDigest digest;
                digest = MessageDigest.getInstance("SHA");
                digest.update(signature.toByteArray());
                KeyHashes = new String(Base64.encode(digest.digest(),0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return  KeyHashes;
    }
}
