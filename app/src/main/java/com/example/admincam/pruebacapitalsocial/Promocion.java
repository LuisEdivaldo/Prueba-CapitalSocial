package com.example.admincam.pruebacapitalsocial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Promocion extends AppCompatActivity {

    private ImageView principal, logotipo;
    private TextView titulo, promocion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promocion);

        mostrarContenido();

    }

    private void mostrarContenido(){
        Bundle bundle = getIntent().getExtras();

        principal = (ImageView) findViewById(R.id.imgPromocion);
        logotipo = (ImageView) findViewById(R.id.imgLogo);
        titulo = (TextView) findViewById(R.id.txtNombreEmpresa);
        promocion = (TextView) findViewById(R.id.txtPromocion);

        principal.setImageResource(bundle.getInt("idImage"));
        logotipo.setImageResource(bundle.getInt("idImage"));
        titulo.setText(bundle.getString("titulo"));
        promocion.setText(bundle.getString("promocion"));


    }
}
