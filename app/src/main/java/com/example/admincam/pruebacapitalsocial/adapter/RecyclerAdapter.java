package com.example.admincam.pruebacapitalsocial.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admincam.pruebacapitalsocial.Login;
import com.example.admincam.pruebacapitalsocial.Promocion;
import com.example.admincam.pruebacapitalsocial.Promociones;
import com.example.admincam.pruebacapitalsocial.R;
import com.example.admincam.pruebacapitalsocial.model.Landscape;

import java.util.List;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>{

    private static final String TAG = RecyclerAdapter.class.getSimpleName();


    private List<Landscape> mData;
    private LayoutInflater mInflater;

    public RecyclerAdapter(Context context, List<Landscape> data){
        this.mData = data;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder");
        View view = mInflater.inflate(R.layout.recicley_view_items, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder" + position);

        Landscape currentObj = mData.get(position);
        holder.setData(currentObj, position);

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }



    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView marca, descripcion;
        ImageView imgThumb;
        int position;
        Landscape current;

        public MyViewHolder(View itemView) {
            super(itemView);


            imgThumb =  itemView.findViewById(R.id.img_row);
            marca = itemView.findViewById(R.id.marca);
            descripcion = itemView.findViewById(R.id.descripcion);
            itemView.setOnClickListener(this);

        }

        public void setData(Landscape current, int position) {
            this.marca.setText(current.getTitle());
            this.descripcion.setText(current.getDescription());
            this.imgThumb.setImageResource(current.getImageID());
            this.position = position;
            this.current = current;
        }

        @Override
        public void onClick(View view) {


            Intent intent = new Intent(view.getContext(),Promocion.class);
            intent.putExtra("idImage",current.getImageID());
            intent.putExtra("titulo",current.getTitle());
            intent.putExtra("promocion",current.getDescription());

            Toast.makeText(view.getContext(), "Pulsé sobre "+current.getTitle()+"imagen es "+current.getImageID()+" Promocion "+current.getDescription(), Toast.LENGTH_SHORT).show();
            view.getContext().startActivity(intent);
        }
    }
}