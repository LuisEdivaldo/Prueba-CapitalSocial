package com.example.admincam.pruebacapitalsocial.model;

import android.content.DialogInterface;

import java.util.ArrayList;
import com.example.admincam.pruebacapitalsocial.R;


public class Landscape {

    private int imageID;
    private String title;
    private String description;


    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static ArrayList<Landscape> getData(){

        ArrayList<Landscape> dataList = new ArrayList<>();

        int[] images = getImages();
        String [] marca = getMarca();
        String [] promocion = getPromocion();


        for (int i = 0; i < images.length; i++){

            Landscape landscape = new Landscape();
            landscape.setImageID(images[i]);
            //landscape.setTitle("Promoción " + i);
            landscape.setTitle(marca [i]);
            //landscape.setDescription("25 % de decuento");
            landscape.setDescription(promocion [i]);

            dataList.add(landscape);
        }
        return dataList;
    }

    public static int[] getImages(){
        int[] images = {
                R.drawable.promo_benavides, R.drawable.promo_burguer_king,
                R.drawable.promo_chilis, R.drawable.promo_cinepolis,
                R.drawable.promo_idea, R.drawable.promo_italiannis,
                R.drawable.promo_papa_johns, R.drawable.promo_tizoncito,
                R.drawable.promo_wingstop, R.drawable.promo_zona_fitness
        };
        return images;
    }

    public  static String [] getMarca(){
        String [] marca = {
                "Buenavides", "Burger King", "Chillis", "Cinepolis",
                "Idea", "Italianis", "Papa Jhons", "El tizoncito",
                "Wings Stop", "Zona Fitness"
        };
        return marca;
    }

    public static String [] getPromocion(){
        String [] promocion = {
                "25% de descuento ", "30% de descuento", "2 x 1", "10% de descuento",
                "5% de descuento", "2 x 1", "15% de descuento", " 3 x 2", "50% de descuento",
                "10% de descuento"
        };
        return promocion;
    }


}

